export default {
  "trailingSlash": true,
  "pathPrefix": "",
  "titleTemplate": "%s - Kalyn Davis Photography",
  "siteUrl": "https://kalyndavis.com",
  "version": "0.7.11"
}