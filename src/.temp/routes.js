export default [
  {
    path: "/about/",
    component: () => import(/* webpackChunkName: "page--src--pages--about-vue" */ "/Users/kalyn-wrightdavis/Projects/Clients/Hermers Capital/WebProject/HermersCapital/src/pages/About.vue")
  },
  {
    name: "404",
    path: "/404/",
    component: () => import(/* webpackChunkName: "page--node-modules--gridsome--app--pages--404-vue" */ "/Users/kalyn-wrightdavis/Projects/Clients/Hermers Capital/WebProject/HermersCapital/node_modules/gridsome/app/pages/404.vue")
  },
  {
    name: "home",
    path: "/",
    component: () => import(/* webpackChunkName: "page--src--pages--index-vue" */ "/Users/kalyn-wrightdavis/Projects/Clients/Hermers Capital/WebProject/HermersCapital/src/pages/Index.vue")
  },
  {
    name: "*",
    path: "*",
    component: () => import(/* webpackChunkName: "page--node-modules--gridsome--app--pages--404-vue" */ "/Users/kalyn-wrightdavis/Projects/Clients/Hermers Capital/WebProject/HermersCapital/node_modules/gridsome/app/pages/404.vue")
  }
]

