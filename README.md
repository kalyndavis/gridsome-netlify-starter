# Default starter for Gridsome Netlify CMS

This is the project you get when you run `gridsome create new-project` and add Netlify CMS.

### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Install Yarn if you don't have

`npm install --global yarn`

### 3. Run Yarn

1. `yarn` to install dependencies
2. `gridsome develop` to start your dev instance
3. Happy coding 🎉🙌
