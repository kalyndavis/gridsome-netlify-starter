// This is where project configuration and plugin options are located. 
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Gridsome Netlify CMS',
  siteUrl: '',
  siteDescription: 'A Gridsome Project built on Netlify CMS',
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'posts/**/*.md',
        typeName: 'Post',
        remark: {
          // remark options
        }
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'projects/**/*.md',
        typeName: 'Project',
        remark: {
          // remark options
        }
      }
    },
    {
      use: `gridsome-plugin-netlify-cms`,
      options: {
        publicPath: `/admin`,
        pathPrefix: `/admin`        
      }
    },
  ],
  transformers: {
    remark: {
      // global remark options
    }
  },
  chainWebpack: {
    chainWebpack (config) {
      config.mode('development')
    }
  }
}